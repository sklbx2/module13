#include "Helpers.h"

int squareOfSum(int x, int y)
{
    return (x + y) * (x + y);
}

double squareOfSum(double x, double y)
{
    return (x + y) * (x + y);
}
